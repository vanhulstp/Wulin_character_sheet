on('ready', () => {
    on('chat:message', msg => {
        if (msg.type === 'api' && msg.content.includes('!TaiPingParse') && msg.inlinerolls) {
            log('Parsing a Tai Ping roll');
            let charId;
            try {
                charId = /{{id=(.*?)}}/gm.exec(msg.content)[1];
            } catch (e) {
                throw `Error: cannot find the character id.`;
            }

            const charObj = getObj('character', charId);

            let resultBeforeEights = 0;
            let harmony = 0;
            let eights = 0;
            let dice = [];
            let result = 0;
            let success = false;
            let perfect = false;
            msg.inlinerolls.forEach(r => {
                r.results.rolls.forEach(roll => {
                    if (roll.results) {
                        roll.results.forEach(die => {
                            dice.push(die.v);
                            if (die.v === 8) eights += 1;
                            else resultBeforeEights += die.v === 4 ? 0 : die.v;
                        })
                    } else {
                        harmony = -parseInt(roll.expr);
                    }
                })
            })

            if (resultBeforeEights + eights > harmony) {
                /** 
                 * We went overboard. Lose a number of points equal to the difference between the result
                 * and the harmony. Eights are set to zero in any case, to mitigate the loss.
                 **/
                success = true;
                result = resultBeforeEights - harmony;
            } else if (resultBeforeEights + eights === harmony) {
                /**
                 * We succeeded even without taking eights into account.
                 * Perfect success!
                 **/
                success = true;
                result = 0;
                perfect = true;
            } else { 
                /**
                 * Might have failed...
                 **/
                const diff = harmony - resultBeforeEights; // Margin of failure
                const upTo = eights * 8; // Margin offered by the eights. If no eight, it is 0.
                if (upTo >= diff) {
                    /** 
                     * We succeeded thanks to the eights. Perfect success. 
                     **/
                    success = true;
                    result = 0;
                    perfect = true;
                } else {
                    /**
                     * We failed, potentially despite the eights.
                     * Deduce the harmoney from the maximum eights and the results. 
                     * Should be a negative number that indicates the margin of failure. 
                     **/
                    result = resultBeforeEights + upTo - harmony;
                }
            }

            const command = `&{template:roll} {{who=${charObj.attributes.name}}} {{resultBeforeEights=${resultBeforeEights}}} {{harmony=${harmony}}} {{eights=${eights}}} {{dice=${dice}}} {{result=${result}}} {{perfect=${perfect}}} {{success=${success}}}`;

            sendChat(msg.who, command)
        }
    })
})