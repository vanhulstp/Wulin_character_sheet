# Wulin Character Sheet for Roll20
This repository hosts the content you need to play with Wulin on Roll20.

## Features
- Character Sheet in four tabs;
- Automated dice rolls (click to top left serpent seal to roll dice);
- Roll template to display the results in the chat. 

## How to use the content
Right now, using this sheet requires a pro account. If Les Éditions du Troisième Oeil and Roll20 agree, I would like to push the sheet in the public repository for all members to use. 

Under your game, click the "Settings" button, then choose "Game Settings" (Figure 1.1). There, choose "Custom Character Sheet" in the "Character Sheet" dropdown menu at the bottom of the page. It should generate a code area with four tabs. Copy / paste the content of `sheet/wulin.html` into the first tab (HTML Layout). Copy / paste the content of `sheet/wulin.css` into the second tab (CSS). You can then preview the result on the fourth tab (preview) (Figure 1.2).

![Figure 1](assets/images/guide-1.png)

To get the rolls working, you also need to load external scripts. To do so, return on the main page of your game, click "Settings", then "API Scripts" (Figure 2.3). Click the "New Script" tab, give it a name (optionally - you can call it wulin.js) (Figure 2.4), then copy / paste the content of `sheet/wulin-api.js` inside the code area (Figure 2.5). Make sure you save the result, and Roll20 should run it without an error.

![Figure 2](assets/images/guide-2.png)

You are now ready to use this character sheet!

---

# Feuille de personnage Wulin pour Roll20
Ce dépôt héberge le content nécessaire pour jouer à Wulin sur Roll20.

## Fonctionnalités
- Une feuille de personnage en quatre onglets ;
- Des jets de dés automatisés (cliquez sur le sceau de serpent en haut à gauche de la feuille) ;
- Un roll template pour afficher les résultats dans le chat.

## Comment utiliser la feuille
Pour le moment, l'emploi de cette feuille est réservé aux comptes pro. Si Les Éditions du Troisième Oeil et Roll20 y constentent, j'aimerais bien l'envoyer sur le dépôt officiel de la plateforme pour que tout le monde puisse l'utiliser. 

Atteignez la page de votre jeu, cliquez sur "Paramètres", puis choisissez "Paramètres du jeu" (Figure 1.1). De là, choisissez "Custom" dans le menu "Character Sheet". Une zone de code apparaît alors, surplombée de quatre onglets. Copiez et collez le contenu de `sheet/wulin.html` dans le premier onglet (HTML Layout). Faites de même avec le contenu de `sheet/wulin.css`dans le second onglet. Vous pouvez prévisualiser le résultat dans le quatrième onglet (Figure 1.2).

![Figure 1](assets/images/guide-1.png)

Pour faire fonctionner les dés, vous aurez besoin d'utiliser des scripts externes. Pour ce faire, retournez sur la page principale de votre jeu, cliquez à nouveau "Paramètres", puis "Scripts API" (Figure 2.3). Cliquez sur l'onglet "New Script", donnez-lui un nom (wulin.js, par exemple) (Figure 2.4), puis copiez et collez le contenu de `sheet/wulin-api.js` dans la zone de code (Figure 2.5). N'oubliez pas de sauvegarder, et Roll20 devrait lancer le script sans erreur.

![Figure 2](assets/images/guide-2.png)

Votre feuille de personnage est prête !
